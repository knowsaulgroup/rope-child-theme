/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'owlCarousel',
    'domReady!'
], function ($, keyboardHandler) {
    'use strict';

    if ($('body').hasClass('checkout-cart-index')) {
        if ($('#co-shipping-method-form .fieldset.rates').length > 0 && $('#co-shipping-method-form .fieldset.rates :checked').length === 0) {
            $('#block-shipping').on('collapsiblecreate', function () {
                $('#block-shipping').collapsible('forceActivate');
            });
        }
    }


    $('.cart-summary').mage('sticky', {
        container: '#maincontent'
    });


    $('.panel.header .header.links').clone().appendTo('#store\\.links');

    $('[data-action="toggle-menu"]').click(function() {

        if ($('html').hasClass('nav-open')) {
            $('html').removeClass('nav-open');
            setTimeout(function () {
                $('html').removeClass('nav-before-open');
            }, 300);
        } else {
            $('html').addClass('nav-before-open');
            setTimeout(function () {
                $('html').addClass('nav-open');
            }, 42);
        }
    }) ;


    $(".footer-columns .widget ").click(function(){
        $(this).children(".block-content").slideDown();
    });

    $( ".megamenu" ).hover(
        function() {
            $( this ).children(".megamenu-content").css( "display" ,"block");
        }, function() {
            $( this ).children(".megamenu-content").css( "display" ,"none"); ;
        }
    );


    $('.btn-number').click(function(e){
        e.preventDefault();

        var fieldName = $(this).attr('data-field');
        var type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {

                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('maxlength')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('maxlength')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });

    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        var minValue =  parseInt($(this).attr('min'));
        var maxValue =  parseInt($(this).attr('maxlength'));
        var valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
    });

    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    // Luis Hack for Slider. Remove later
    setTimeout(function() {

        $(".preload-fake-slider").hide();

        $(".block-slide").show();

        $(".block-slide").owlCarousel({
            autoPlay : true,
            nav: true,
            navText : ["",""],
            items: 1,
            loop: true,
            dots: true,
            autoplay: true,
            autoplayHoverPause: true,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            lazyLoad: true
        });

    }, 50);


    $(".owl-client-say").owlCarousel({
        navText : ["",""],
        autoPlay : 4000,
        nav:false,
        singleItem : true,
        items: 1
    });
    $(".owl-dt").owlCarousel({
        items: 3,
        nav:false,
    });

    $('.tab-new-category .product-items').addClass('owl-carousel');
    $(".tab-new-category .product-items").owlCarousel({
        items: 4,
        navText : ["",""],
        nav:true,
        responsive:{
            0: {
                items: 1,
            },
            360:{
                items: 2,
            },
            600:{
                items: 3,
            },
            992:{
                items: 4,
            }
        }
    });

    $(".block-blog-style2 .post-list").owlCarousel({
        navText : ["",""],
        autoPlay : 4000,
        nav:false,
        singleItem : true,
        pagination: true,
    });

    $(".block-products.style1 .product-items").owlCarousel({
        pagination: true,
        items: 4,
        slideBy: 4,
        responsive:{
            0: {
                items: 2,
                slideBy: 2,
            },
            600:{
                items: 3,
                slideBy: 3,
            },
            992:{
                items: 4,
                slideBy: 4,
            }
        }
    });

    $(".products-related .product-items, " +
        ".products-upsell .product-items, " +
        ".crosssell .product-items, " +
        ".catalog-category-view .block-viewed-products-grid .product-items, " +
        ".catalog-product-view .block-viewed-products-grid .product-items"
    ).owlCarousel({
        items: 4,
        nav : false,
        loop: true,
        slideBy: 4,
        responsive:{
            0: {
                items: 2,
                slideBy: 2,
            },
            600:{
                items: 3,
                slideBy: 3,
            },
            992:{
                items: 4,
                slideBy: 4,
            }
        }
    });

    // $( '.authorization-link a' ).text('Log In');

    var flag = $('ul.switcher-options img').attr('src');

    $( '.switcher-label' ).html('<strong title="US Stores"> <img src="'+flag+'"> <span class="country-switcher">US</span> <span class="store-view"><a href="/customer/account/">My Account</a></span></strong>');

    $( '.country-switcher, .switcher-options' ).on('hover', function() {
        $( '.switcher-options' ).addClass('show');
    });

    $( '.header-top, .chat-box, .store-view, .homepage-slider-section' ).on('hover', function() {
        $( '.switcher-options' ).removeClass('show');
    });

    //Double tab

    /*
  By Osvaldas Valutis, www.osvaldas.info
  Available for use under the MIT License
  */

    ;(function( $, window, document, undefined )
    {
        $.fn.doubleTapToGo = function( params )
        {
            if( !( 'ontouchstart' in window ) &&
                !navigator.msMaxTouchPoints &&
                !navigator.userAgent.toLowerCase().match( /windows phone os 7/i ) ) return false;

            this.each( function()
            {
                var curItem = false;

                $( this ).on( 'click', function( e )
                {
                    var item = $( this );
                    if( item[ 0 ] != curItem[ 0 ] )
                    {
                        e.preventDefault();
                        curItem = item;
                    }
                });

                $( document ).on( 'click touchstart MSPointerDown', function( e )
                {
                    var resetItem = true,
                        parents	  = $( e.target ).parents();

                    for( var i = 0; i < parents.length; i++ )
                        if( parents[ i ] == curItem[ 0 ] )
                            resetItem = false;

                    if( resetItem )
                        curItem = false;
                });
            });
            return this;
        };
    })( jQuery, window, document );

    // Only activate on Mobile viewports

    var $mobile = $( window ).width();

    if ( $mobile < 1081 ) {

        $('.parentMenu > a').on( 'click', function(e) {

            e.preventDefault();

            // I was trying to create an accordining effect where one opens, and the slibling closes.
            // $('.parentMenu .menu-box').hide();

            $(this).parents('.parentMenu').find('.menu-box').toggle();
        });

        // Create mobile dropdown
        $( '.parentMenu .block-main > .grid-child >.itemsubmenu > .subparent > a' ).on( 'click', function(e) {

            if ( $(this).siblings().is( '.itemsubmenu' ) ) {
                e.preventDefault();
            }

            // I was trying to create an accordining effect where one opens, and the slibling closes.
            // $('.magemenu-menu ul.itemsubmenu .itemsubmenu').hide();

            $(this).parents('.parentMenu .block-main > .grid-child >.itemsubmenu > .subparent').find('.itemsubmenu').toggle();

        } );

    }

    $(window).resize(function() {

        var $size = $( window ).width();

        if ( $size < 1081 ) {

            $('.parentMenu > a').on( 'click', function(e) {
                e.preventDefault();

                $(this).parents('.parentMenu').find('.menu-box').toggle();

            } );

            // Create mobile dropdown
            $( '.parentMenu .block-main > .grid-child >.itemsubmenu > .subparent > a' ).on( 'click', function(e) {

                if ( $(this).siblings().is( '.itemsubmenu' ) ) {
                    e.preventDefault();
                }

                $(this).parents('.parentMenu .block-main > .grid-child >.itemsubmenu > .subparent').find('.itemsubmenu').toggle();

            } );

        } else if ( $size > 1080 ) {

            $( '.parentMenu > a' ).unbind( "click" );

            $( '.parentMenu .block-main > .grid-child >.itemsubmenu > .subparent > a' ).unbind( "click" );

        }
    });

    // Add class to sale link
    $( '.magemenu-menu.horizontal-menu ul.explodedmenu li.menu > a > span:contains(\'SALE\')' ).addClass( 'sale-link' );

    $(".fullwidth-triple-product__container").owlCarousel({
        items: 4,
        navText : ["",""],
        margin: 24,
        responsive:{
            0:{
                items: 1,
                loop:true,
            },
            600:{
                items: 2,
                loop:true,
            },
            769:{
                items: 3,
                loop:true,
            },
            1081:{
                items: 4,
                nav: false,
                mouseDrag: false
            }
        }
    });



    // Carousel for product widgets
    $( ".product-widget-section .product-items" ).owlCarousel({
        items: 4,
        loop:true,
        // autoWidth: true,
        // margin: 10,
        responsive:{
            0: {
                items: 2,
            },
            600:{
                items: 3,
            },
            992:{
                items: 4,
            }
        }
    });


    // Add overlay to the product widgets on the homepage
    $( '.product-widget-section .product-item-info > a, ' +
        '.product-item-header > a, ' +
        '.admin__data-grid-outer-wrap .product-item-info > a, ' +
        '.feature-product-section .product-item-info > a'
    ).append( '<div class="fullwidth-triple-product__overlay"><span class="fullwidth-triple-product__shop-now">Shop Now</span></div>' );

    // Wait for ajax to finish to load my code
    setTimeout( function() {
        $( ".shop-now-overlay .product-item-info > a" ).append( '<div class="fullwidth-triple-product__overlay"><span class="fullwidth-triple-product__shop-now">Shop Now</span></div>' );
    }, 120000);

    // Move the newsletter inside the footer
    $( '.footer-info__title:last' ).after( $( '.block.newsletter' ) );


    // Hide the Nic level on the product pages
    $('[data-th="Nic Level"]').parent().hide();

    // Satic Code
    // When the page resizes
    $( '.category-view' ).wrap( "<li id='category-view-image'></li>" );


    // Move toolbar to page title container
    $( '.category-title-custom .box' ).after( $( '.page-products .toolbar' ).eq(0) );

    // $( '.page-products .sorter' ).before( $( '#layered-filter-block' ) );

    var $size = $( window ).width();

    if ( $size > 769 ) {
        // Move catalog image into the product grid
        $( '#category-view-image' ).insertBefore( $( '.item.product.product-item:first' ) );
    }


    $(window).resize(function() {
        // Moving catalog elements around
        var $size = $( window ).width();

        if ( $size > 769 ) {
            // Move catalog image into the product grid
            $( '#category-view-image' ).insertBefore( $( '.item.product.product-item:first' ) );
        }
    });

    $(window).resize(function() {
        var $size2 = $( window ).width();

        if ( $size2 < 768 ) {
            $( '#category-view-image' ).prependTo( $( '.column.main' ) );
        }
    });

    //Reverse place with with price
    $( '.catalog-product-view:not(.page-product-grouped) .product-info-main .product-info-stock-sku').insertBefore($('.catalog-product-view:not(.page-product-grouped) .product-info-main .product-info-price .price-box'));


    $(document).on('mouseover', '.fotorama__nav__frame', function () {
        var $fotorama = $(this).parents('.fotorama');
        $fotorama.data('fotorama').show({index: $('.fotorama__nav__frame', $fotorama).index(this)});
    });

    // Move bundle configurables into sidebar
    // $( '#bundleSummary' ).insertBefore( '.product-options-wrapper' );

    // Remove the first group bundle filter name. It repeats on the page.
    $('.table.grouped .product-item-name').first().addClass('hide');

    //Add phone number attribute to href
    $('.phone-box').attr('href', 'tel:1-833-827-7966');

    $('.table-wrapper .price-box.price-final_price:first').addClass('first-price');

    $('td.col.qty:first').addClass('first-qty');

    // Create the add to cart button
    $( "<button type=\"submit\" title=\"Add to Cart\" class=\"action primary tocart btn-copy\"><span>Add to Cart</span></button>" ).appendTo( ".bss-gpo-child-product-info:first" );



    // Add message under the product page button
    let url = window.location.protocol + "//" + window.location.hostname;

    // Reomve the if condition once live.
    // if ( url !== 'https://www.vaprzon.com' ) {

    $( "<p class=\"message-text\">Add these discounted items on this purchase only.</p>" ).insertAfter( ".btn-copy" );
    // }

    // Wait for ajax to finish to load my code
    setTimeout( function() {
        // Switch main product on the selected swatch image
        jQuery( ".swatch-attribute-options div" ).each(function() {
            jQuery( this ).on('click', function() {
                if ( jQuery(this).is( '.swatch-option.image') ) {

                    var imgAttr = jQuery(this).attr('style');

                    var imgRemoveExtra = imgAttr.match(/([a-z]+\:\/+)([^\/\s]*)([a-z0-9\-@\^=%&;\/~\+]*)[\?]?([^ \#]*)#?([^ \#]jpg|png)/gi);

                    jQuery('.catalog-product-view .fotorama__stage__frame.fotorama__active .fotorama__img').attr('src', imgRemoveExtra);

                    console.log('swatches images are being changed');
                }
            });
        });
    }, 10000);

    // // Move product detail inside of the image gallery div on desktop view
    // if ( $(window).width() > 768) {
    //
    //     // $( ".page-product-grouped .product.media" ).append( $('.page-product-grouped .product.detailed ') );
    //
    // }
    //
    // $( window ).on('resize', function () {
    //
    //     if ( $(window).width() > 768) {
    //
    //         // $( ".page-product-grouped .product.media" ).append($('.page-product-grouped .product.detailed ') );
    //
    //     } else {
    //
    //         $( '.page-product-grouped .main > .container' ).eq(0).after( $('.page-product-grouped .product.detailed ') );
    //     }
    //
    // });

    // Disable Device products sidebar extra
    $( '.page-product-grouped #super-product-table tbody, #product-addtocart-button' ).addClass('noshow')
    $( '.page-product-grouped #super-product-table tbody' ).eq(0).removeClass('noshow');
    $('.page-product-grouped #product-addtocart-button').prop("disabled", true);

    $('.page-product-grouped .noshow').fadeTo('slow',.6);
    $('.page-product-grouped .noshow').append('<div class="disable-div"></div>');
    $('.page-product-grouped .message-text').css('color', '#cac5c5');



    $('.page-product-grouped .btn-copy, .page-product-grouped #product-addtocart-button').on('click', function () {

        if ( $( ".page-product-grouped .first-qty input" ).val() !== '0' && $('.page-product-grouped .swatch-attribute-options .swatch-option').is('.selected') ) {

            $('.page-product-grouped .noshow, .message-text').removeAttrs('style');
            $('.page-product-grouped .noshow .disable-div').hide();
            $('.page-product-grouped #product-addtocart-button').prop("disabled", false);
            $('.page-product-grouped .noshow .price-box.price-final_price .price').css('color', '#ff5353');

            setTimeout(function () {

                $(".catalog-product-view.page-product-grouped .table.grouped .control .qty").val(0);

            }, 500);

        } else {

            $( '.catalog-product-view.page-product-grouped .table.grouped .control .qty' ).each( function() {

                if ( $( this.value ).val() !== '0' ) {

                    setTimeout(function () {

                        $(".catalog-product-view.page-product-grouped .table.grouped .control .qty").val(0);

                    }, 500);
                }

            });
        }
    });

    //     Don't display second at to cart button is no options are there

    if ( !$('tbody').is('.noshow') ) {
        $( ".page-product-grouped .message-text" ).hide();
        $( '.page-product-grouped .product-info-main .box-tocart' ).hide();

    }

    // Apply class is List Price is showing
    if ( $('.first-price .price-container').is('.list-price-text') ) {
        $( '.first-qty' ).addClass( 'list-price-is-showing' );
    }

    // Add Mobile My icon button.
    $( '.minicart-wrapper' ).prepend( '<a class="mobile-only-account-icon" href="/customer/account/"></a>' );



    setTimeout( function() {

        // Expand cart tabs on first page load.
        $( '.checkout-cart-index #block-discount, .checkout-cart-index #block-shipping' ).addClass( 'active' );

        $( '.checkout-cart-index #block-discount > .title, .checkout-cart-index #block-shipping > .title' ).attr( 'aria-expanded', 'true' ).attr( 'aria-selected', 'true' );

        $( '.checkout-cart-index #block-discount > .content, .checkout-cart-index #block-shipping  > .content' ).attr( 'aria-hidden', 'false' );

        $( '#block-discount' ).addClass( 'pulse-in-checkout' );

    }, 500);

    // Popup for the cart page
    setTimeout( function() {
        if ( checkoutConfig.totalsData.grand_total ) {
            let getTheCartTotal = checkoutConfig.totalsData.grand_total;
        }

        if ( getTheCartTotal < 50 ) {

            $(window).scrollTop(0);

            $( 'body.checkout-cart-index, body.checkout-index-index' ).attr( 'style', 'overflow: hidden;' );

            $( '.cart-popup-overlady' ).show();
        }


        $( '.cart-popup__btn' ).click( function () {

            $( 'body.checkout-cart-index, body.checkout-index-index' ).removeAttr( 'style' );

            $( '.cart-popup-overlady' ).hide();

        });

    }, 500);


    keyboardHandler.apply();

});
