define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('bss.groupedQty', {
        _create: function () {
            var $widget = this;
            $widget.element.find('.btn-number').click(function () {
                var $qtyBox = $(this).parents('.control').find('.qty');
                if ($(this).hasClass('btn-minus')) {
                    $widget.minusQty($qtyBox);
                }
                if ($(this).hasClass('btn-plus')) {
                    $widget.plusQty($qtyBox);
                }
            });
        },
        minusQty: function ($qtyBox) {
            var value = Number($qtyBox.val());
            if (value > 0) {
                $qtyBox.val(value - 1).trigger('change');
            }
        },
        plusQty: function ($qtyBox) {
            var value = Number($qtyBox.val());
            $qtyBox.val(value + 1).trigger('change');
        }
    });

    return $.bss.groupedQty;
});
